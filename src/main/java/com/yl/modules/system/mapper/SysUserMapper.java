package com.yl.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yl.modules.system.entity.SysUser;

/**
 * @author yl
 * @description
 * @date 2021/09/23 17:39
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
