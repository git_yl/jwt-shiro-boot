package com.yl.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yl.modules.system.entity.SysPermission;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yl
 * @description
 * @date 2021/09/23 17:39
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    @Select("SELECT p.*\n" +
            "FROM test.sys_permission p\n" +
            "WHERE (exists(\n" +
            "               select a.id\n" +
            "               from sys_role_permission a\n" +
            "                        join sys_role b on a.role_id = b.id\n" +
            "                        join sys_user_role c on c.role_id = b.id\n" +
            "                        join sys_user d on d.id = c.user_id\n" +
            "               where p.id = a.permission_id\n" +
            "                 AND d.username = #{username}\n" +
            "           )\n" +
            "    or (p.url like '%:code' and p.url like '/online%' and p.hidden = 1)\n" +
            "    )\n" +
            "  and p.del_flag = 0\n" +
            "order by p.sort_no ASC")
    List<SysPermission> queryUserAuths(@Param("username")String username);
}
