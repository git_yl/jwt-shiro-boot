package com.yl.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yl.modules.system.entity.SysRolePermission;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @Author scott
 * @since 2018-12-19
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
