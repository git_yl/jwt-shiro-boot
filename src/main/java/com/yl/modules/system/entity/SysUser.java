package com.yl.modules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yl
 * @description
 * @date 2021/09/23 16:38
 */
@Data
@TableName("sys_user")
@ApiModel(value = "sys_user",description = "用户表")
public class SysUser implements Serializable {
    /**
     * 登录人id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 登录人账号
     */
    private String username;

    /**
     * 登录人名字
     */
    private String realname;

    /**
     * 登录人密码
     */
    private String password;

    /**
     * md5密码盐
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)//这个注解的意思是返回参数加在内
    private String salt;
}
