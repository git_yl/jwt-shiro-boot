package com.yl.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yl.modules.system.dto.LoginUser;
import com.yl.modules.system.entity.SysUser;

/**
 * @author yl
 * @description
 * @date 2021/09/09 18:20
 */
public interface ISysUserService extends IService<SysUser> {

    LoginUser getUserByName(String username);

    SysUser queryByUsername(String username);
}
