package com.yl.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yl.modules.system.dto.LoginUser;
import com.yl.modules.system.entity.SysUser;
import com.yl.modules.system.mapper.SysUserMapper;
import com.yl.modules.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * @author yl
 * @description
 * @date 2021/09/09 18:20
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {


    @Override
    public LoginUser getUserByName(String username) {
        LambdaUpdateWrapper<SysUser> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(SysUser::getUsername, username)
                .last(" limit 1");
        SysUser user = getOne(wrapper);
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(user, loginUser);
        return loginUser;
    }

    @Override
    public SysUser queryByUsername(String username) {
        LambdaUpdateWrapper<SysUser> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(SysUser::getUsername, username)
                .last(" limit 1");
        return getOne(wrapper);
    }
}
