package com.yl.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yl.modules.system.entity.SysPermission;
import com.yl.modules.system.mapper.SysPermissionMapper;
import com.yl.modules.system.service.ISysPermissionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @Author scott
 * @since 2018-12-21
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    @Override
    public Set<String> queryUserAuths(String username) {
        Set<String> permissionSet = new HashSet<>();
        List<SysPermission> sysPermissions = baseMapper.queryUserAuths(username);
        if (sysPermissions != null && sysPermissions.size() > 0) {
            sysPermissions.stream().forEach(item -> {
                if (StringUtils.isNotBlank(item.getPerms())) {
                    permissionSet.add(item.getPerms());
                }
            });
        }
        return permissionSet;
    }
}
