package com.yl.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yl.modules.system.entity.SysPermission;

import java.util.Set;

/**
 * @author yl
 * @description
 * @date 2021/09/09 18:20
 */
public interface ISysPermissionService extends IService<SysPermission> {

    Set<String> queryUserAuths(String username);
}
