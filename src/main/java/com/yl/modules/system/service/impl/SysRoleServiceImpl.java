package com.yl.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yl.modules.system.entity.SysRole;
import com.yl.modules.system.mapper.SysRoleMapper;
import com.yl.modules.system.service.ISysRoleService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @Author scott
 * @since 2018-12-19
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    @Override
    public Set<String> queryUserRoles(String username) {
        return new HashSet<>(baseMapper.getRoleByUserName(username));
    }
}
