package com.yl.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yl.modules.system.entity.SysRole;

import java.util.Set;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @Author scott
 * @since 2018-12-19
 */
public interface ISysRoleService extends IService<SysRole> {

    Set<String>  queryUserRoles(String username);

}
