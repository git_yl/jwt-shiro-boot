/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 127.0.0.1:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 24/09/2021 14:57:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父id',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `component_name` varchar(100) DEFAULT NULL COMMENT '组件名字',
  `redirect` varchar(255) DEFAULT NULL COMMENT '一级菜单跳转地址',
  `menu_type` int(11) DEFAULT NULL COMMENT '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
  `perms` varchar(255) DEFAULT NULL COMMENT '菜单权限编码',
  `perms_type` varchar(10) DEFAULT '0' COMMENT '权限策略1显示2禁用',
  `sort_no` double(8,2) DEFAULT NULL COMMENT '菜单排序',
  `always_show` tinyint(1) DEFAULT NULL COMMENT '聚合子路由: 1是0否',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单图标',
  `is_route` tinyint(1) DEFAULT '1' COMMENT '是否路由菜单: 0:不是  1:是（默认值1）',
  `is_leaf` tinyint(1) DEFAULT NULL COMMENT '是否叶子节点:    1:是   0:不是',
  `keep_alive` tinyint(1) DEFAULT NULL COMMENT '是否缓存该页面:    1:是   0:不是',
  `hidden` int(2) DEFAULT '0' COMMENT '是否隐藏路由: 0否,1是',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除状态 0正常 1已删除',
  `rule_flag` int(3) DEFAULT '0' COMMENT '是否添加数据权限1是0否',
  `status` varchar(2) DEFAULT NULL COMMENT '按钮权限状态(0无效1有效)',
  `internal_or_external` tinyint(1) DEFAULT NULL COMMENT '外链菜单打开方式 0/内部打开 1/外部打开',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_prem_pid` (`parent_id`) USING BTREE,
  KEY `index_prem_is_route` (`is_route`) USING BTREE,
  KEY `index_prem_is_leaf` (`is_leaf`) USING BTREE,
  KEY `index_prem_sort_no` (`sort_no`) USING BTREE,
  KEY `index_prem_del_flag` (`del_flag`) USING BTREE,
  KEY `index_menu_type` (`menu_type`) USING BTREE,
  KEY `index_menu_hidden` (`hidden`) USING BTREE,
  KEY `index_menu_status` (`status`) USING BTREE,
  KEY `idx_sp_parent_id` (`parent_id`) USING BTREE,
  KEY `idx_sp_is_route` (`is_route`) USING BTREE,
  KEY `idx_sp_is_leaf` (`is_leaf`) USING BTREE,
  KEY `idx_sp_sort_no` (`sort_no`) USING BTREE,
  KEY `idx_sp_del_flag` (`del_flag`) USING BTREE,
  KEY `idx_sp_menu_type` (`menu_type`) USING BTREE,
  KEY `idx_sp_hidden` (`hidden`) USING BTREE,
  KEY `idx_sp_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission` VALUES ('00a2a0ae65cdca5e93209cdbde97cbe6', '2e42e3835c2b44ec9f7bc26c146ee531', '成功', '/result/success', 'result/Success', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('020b06793e4de2eee0007f603000c769', 'f0675b52d89100ee88472b6800754a08', 'ViserChartDemo', '/report/ViserChartDemo', 'jeecg/report/ViserChartDemo', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-03 19:08:53', 'admin', '2019-04-03 19:08:53', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('024f1fd1283dc632458976463d8984e1', '700b7f95165c46cc7a78bf227aa8fed3', 'Tomcat信息', '/monitor/TomcatInfo', 'modules/monitor/TomcatInfo', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 09:44:29', 'admin', '2019-05-07 15:19:10', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('043780fa095ff1b2bec4dc406d76f023', '2a470fc0c3954d9dbb61de6d80846549', '表格合计', '/jeecg/tableTotal', 'jeecg/TableTotal', NULL, NULL, 1, NULL, '1', 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-08-14 10:28:46', NULL, NULL, 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('05b3c82ddb2536a4a5ee1a4c46b5abef', '540a2936940846cb98114ffb0d145cb8', '用户列表', '/list/user-list', 'examples/list/UserList', NULL, NULL, 1, NULL, NULL, 3.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('0620e402857b8c5b605e1ad9f4b89350', '2a470fc0c3954d9dbb61de6d80846549', '异步树列表Demo', '/jeecg/JeecgTreeTable', 'jeecg/JeecgTreeTable', NULL, NULL, 1, NULL, '0', 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-05-13 17:30:30', 'admin', '2019-05-13 17:32:17', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('078f9558cdeab239aecb2bda1a8ed0d1', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（文章）', '/list/search/article', 'examples/list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-12 14:00:34', 'admin', '2019-02-12 14:17:54', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('08e6b9dc3c04489c8e1ff2ce6f105aa4', '1363787175921025026', '系统监控', '/dashboard3', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 6.00, 0, 'dashboard', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:48:15', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('0ac2ad938963b6c6d1af25477d5b8b51', '8d4683aacaa997ab86b966b464360338', '代码生成按钮', NULL, NULL, NULL, NULL, 2, 'online:goGenerateCode', '1', 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-06-11 14:20:09', NULL, NULL, 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('109c78a583d4693ce2f16551b7786786', 'e41b69c57a941a3bbcce45032fe57605', 'Online报表配置', '/online/cgreport', 'modules/online/cgreport/OnlCgreportHeadList', NULL, NULL, 1, NULL, NULL, 2.00, 0, 'file-text', 1, 1, 0, 0, NULL, 'admin', '2019-03-08 10:51:07', 'admin', '2021-03-24 14:12:57', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('1166535831146504193', '2a470fc0c3954d9dbb61de6d80846549', '对象存储', '/oss/file', 'modules/oss/OSSFileList', NULL, NULL, 1, NULL, '1', 1.00, 0, '', 1, 1, 0, 0, NULL, 'admin', '2019-08-28 02:19:50', 'admin', '2019-08-28 02:20:57', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('1170592628746878978', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '菜单管理2', '/isystem/newPermissionList', 'system/NewPermissionList', NULL, NULL, 1, NULL, '1', 100.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-09-08 15:00:05', 'admin', '2019-12-25 09:58:18', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1174506953255182338', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '职务管理', '/isystem/position', 'system/SysPositionList', NULL, NULL, 1, NULL, '1', 2.00, 0, 'idcard', 1, 1, 0, 0, NULL, 'admin', '2019-09-19 10:14:13', 'admin', '2021-03-24 10:13:09', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1174590283938041857', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '通讯录', '/isystem/addressList', 'system/AddressList', NULL, NULL, 1, NULL, '1', 3.00, 0, 'copy', 1, 1, 0, 0, NULL, 'admin', '2019-09-19 15:45:21', 'admin', '2021-03-24 10:17:01', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1192318987661234177', 'e41b69c57a941a3bbcce45032fe57605', '系统编码规则', '/isystem/fillRule', 'system/SysFillRuleList', NULL, NULL, 1, NULL, '1', 3.00, 0, 'code', 1, 1, 0, 0, NULL, 'admin', '2019-11-07 13:52:53', 'admin', '2021-03-24 14:13:15', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1205097455226462210', '1363786004946845697', '大屏设计', '/big/screen', 'layouts/RouteView', NULL, NULL, 1, NULL, '1', 1.20, 0, 'area-chart', 1, 0, 0, 0, NULL, 'admin', '2019-12-12 20:09:58', 'admin', '2021-02-22 17:49:11', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1205098241075453953', '1205097455226462210', '生产销售监控', '{{ window._CONFIG[\'domianURL\'] }}/test/bigScreen/templat/index1', 'layouts/IframePageView', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-12-12 20:13:05', 'admin', '2019-12-12 20:15:27', 0, 0, '1', 1);
INSERT INTO `sys_permission` VALUES ('1205306106780364802', '1205097455226462210', '智慧物流监控', '{{ window._CONFIG[\'domianURL\'] }}/test/bigScreen/templat/index2', 'layouts/IframePageView', NULL, NULL, 1, NULL, '1', 2.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-12-13 09:59:04', 'admin', '2019-12-25 09:28:03', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1209731624921534465', 'e41b69c57a941a3bbcce45032fe57605', '多数据源管理', '/isystem/dataSource', 'system/SysDataSourceList', NULL, NULL, 1, NULL, '1', 6.00, 0, 'database', 1, 1, 0, 0, NULL, 'admin', '2019-12-25 15:04:30', 'admin', '2021-03-24 14:14:31', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1224641973866467330', 'e41b69c57a941a3bbcce45032fe57605', '系统校验规则', '/isystem/checkRule', 'system/SysCheckRuleList', NULL, NULL, 1, NULL, '1', 5.00, 0, 'build', 1, 1, 0, 0, NULL, 'admin', '2019-11-07 13:52:53', 'admin', '2021-03-24 14:14:08', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1229674163694841857', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线表单ERP', '/online/cgformErpList/:code', 'modules/online/cgform/auto/erp/OnlCgformErpList', NULL, NULL, 1, NULL, '1', 5.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2020-02-18 15:49:00', 'admin', '2020-02-18 15:52:25', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1232123780958064642', 'f0675b52d89100ee88472b6800754a08', 'Online报表示例', '/online/cgreport/6c7f59741c814347905a938f06ee003c', 'modules/online/cgreport/auto/OnlCgreportAutoList', NULL, NULL, 1, NULL, '1', 4.00, 0, NULL, 0, 1, 0, 0, NULL, 'admin', '2020-02-25 10:02:56', 'admin', '2020-05-02 15:37:30', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1235823781053313025', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线内嵌子表', '/online/cgformInnerTableList/:code', 'modules/online/cgform/auto/innerTable/OnlCgformInnerTableList', NULL, NULL, 1, NULL, '1', 999.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2020-03-06 15:05:24', 'admin', '2020-03-06 15:07:42', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260922988733255681', '2a470fc0c3954d9dbb61de6d80846549', 'online订单管理', '/online/cgformInnerTableList/56efb74326e74064b60933f6f8af30ea', '111111', NULL, NULL, 1, NULL, '1', 11.00, 0, NULL, 0, 1, 0, 0, NULL, 'admin', '2020-05-14 21:20:42', 'admin', '2020-09-09 15:31:48', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260923256208216065', '2a470fc0c3954d9dbb61de6d80846549', 'online用户报表', '/online/cgreport/1260179852088135681', '333333', NULL, NULL, 1, NULL, '1', 11.00, 0, NULL, 0, 1, 0, 0, NULL, 'admin', '2020-05-14 21:21:46', 'admin', '2020-09-09 15:31:54', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260928341675982849', '3f915b2769fc80648e92d04e84ca059d', '添加按钮', NULL, NULL, NULL, NULL, 2, 'user:add', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 21:41:58', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260929666434318338', '3f915b2769fc80648e92d04e84ca059d', '用户编辑', NULL, NULL, NULL, NULL, 2, 'user:edit', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 21:47:14', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260931366557696001', '3f915b2769fc80648e92d04e84ca059d', '表单性别可见', '', NULL, NULL, NULL, 2, 'user:sex', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 21:53:59', 'admin', '2020-05-14 21:57:00', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1260933542969458689', '3f915b2769fc80648e92d04e84ca059d', '禁用生日字段', NULL, NULL, NULL, NULL, 2, 'user:form:birthday', '2', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-05-14 22:02:38', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1265162119913824258', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '路由网关', '/isystem/gatewayroute', 'system/SysGatewayRouteList', NULL, NULL, 1, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, NULL, NULL, '2020-05-26 14:05:30', 'admin', '2020-09-09 14:47:52', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1280350452934307841', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '租户管理', '/isys/tenant', 'system/TenantList', NULL, NULL, 1, NULL, '1', 10.00, 0, 'contacts', 1, 1, 0, 0, NULL, 'admin', '2020-07-07 11:58:30', 'admin', '2021-03-24 10:11:50', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1280464606292099074', '2a470fc0c3954d9dbb61de6d80846549', '图片裁剪', '/jeecg/ImagCropper', 'jeecg/ImagCropper', NULL, NULL, 1, NULL, '1', 9.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-07 19:32:06', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1287715272999944193', '2a470fc0c3954d9dbb61de6d80846549', 'JVXETable示例', '/jeecg/j-vxe-table-demo', 'layouts/RouteView', NULL, NULL, 1, NULL, '1', 0.10, 0, '', 1, 0, 0, 0, NULL, 'admin', '2020-07-27 19:43:40', 'admin', '2020-09-09 14:52:06', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1287715783966834689', '1287715272999944193', '普通示例', '/jeecg/j-vxe-table-demo/normal', 'jeecg/JVXETableDemo', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-27 19:45:42', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1287716451494510593', '1287715272999944193', '布局模板', '/jeecg/j-vxe-table-demo/layout', 'jeecg/JVxeDemo/layout-demo/Index', NULL, NULL, 1, NULL, '1', 2.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-27 19:48:21', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1287718919049691137', '1287715272999944193', '即时保存', '/jeecg/j-vxe-table-demo/jsbc', 'jeecg/JVxeDemo/demo/JSBCDemo', NULL, NULL, 1, NULL, '1', 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-27 19:57:36', 'admin', '2020-07-27 20:03:37', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1287718938179911682', '1287715272999944193', '弹出子表', '/jeecg/j-vxe-table-demo/tczb', 'jeecg/JVxeDemo/demo/PopupSubTable', NULL, NULL, 1, NULL, '1', 4.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-27 19:57:41', 'admin', '2020-07-27 20:03:47', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1287718956957810689', '1287715272999944193', '无痕刷新', '/jeecg/j-vxe-table-demo/whsx', 'jeecg/JVxeDemo/demo/SocketReload', NULL, NULL, 1, NULL, '1', 5.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2020-07-27 19:57:44', 'admin', '2020-07-27 20:03:57', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1304032910990495745', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线表单TAB', '/online/cgformTabList/:code', 'modules/online/cgform/auto/tab/OnlCgformTabList', NULL, NULL, 1, NULL, '1', 8.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2020-09-10 20:24:08', 'admin', '2020-09-10 20:36:37', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('13212d3416eb690c2e1d5033166ff47a', '2e42e3835c2b44ec9f7bc26c146ee531', '失败', '/result/fail', 'result/Error', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('1352494133276602370', '', '业务管理', '/zhgl', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 1.00, 0, 'appstore', 1, 0, 0, 0, NULL, 'admin', '2021-01-22 13:51:44', 'admin', '2021-04-13 17:42:52', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1352494718902104065', '1352494133276602370', '工作台', '/zhgl/working', 'zhgl/working', NULL, NULL, 1, NULL, '1', 20.00, 0, 'desktop', 1, 0, 0, 1, NULL, 'admin', '2021-01-22 13:54:04', 'admin', '2021-04-13 17:45:35', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1353594404652355585', '1352494133276602370', '发起工单', '/zhgl/addOrder', 'zhgl/addOrder', NULL, NULL, 1, NULL, '1', 20.00, 0, 'form', 1, 1, 0, 1, NULL, 'admin', '2021-01-25 14:43:49', 'admin', '2021-04-13 17:50:31', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1354261109644881922', '1352494133276602370', '场所详情', '/zhgl/siteDetails', 'zhgl/siteDetails', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-27 10:53:04', 'admin', '2021-02-22 17:29:58', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1354348862730563586', '1352494133276602370', '场所查询', '/placeManage/placeSearch/EpmsEplaceList', 'dataAccount/place/EpmsEplaceList', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-27 16:41:46', 'admin', '2021-03-25 09:40:12', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1354349444845432834', '1352494133276602370', '被查处场所', '/placeManage/placeCheck/EpmsEplaceInvestigateList', 'placeManage/placeCheck/EpmsEplaceInvestigateList', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-27 16:44:05', 'admin', '2021-02-22 17:25:35', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1354350143213826050', '1352494133276602370', '场所台账', '/placeManage/placeCheCaseLog/EpmsPlaceCaseList', 'placeManage/placeCheCaseLog/EpmsPlaceCaseList', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-27 16:46:51', 'admin', '2021-02-22 17:26:02', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1354666781633048577', '1352494133276602370', '备案登记', '/placeManage/placeSearch/addRecord', 'placeManage/placeSearch/addRecord', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-28 13:45:04', 'admin', '2021-04-13 17:47:13', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355066391568478209', '1352494133276602370', '民警分配', '/placeManage/placePolice/index', 'placeManage/placePolice/index', NULL, NULL, 1, NULL, '1', 10.00, 0, 'bank', 1, 0, 0, 0, NULL, 'admin', '2021-01-29 16:12:58', 'admin', '2021-04-16 16:02:04', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355066910240305153', '1352494133276602370', '长期未上报场所', '/placeManage/longNoRepPlace/index', 'placeManage/longNoRepPlace/index', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-29 16:15:02', 'admin', '2021-02-22 17:27:17', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355067136367816706', '1352494133276602370', '长期未检查场所', '/placeManage/longNoChePlace/index', 'placeManage/longNoChePlace/index', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-29 16:15:56', 'admin', '2021-07-26 15:54:45', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355067548076503042', '1352494133276602370', '数据不完整场所', '/placeManage/dataNoCompPlace/index', 'placeManage/dataNoCompPlace/index', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-29 16:17:34', 'admin', '2021-02-22 17:27:49', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355067790859595778', '1352494133276602370', '有警情场所', '/placeManage/alertPlace/EpmsPoliceInformationList', 'placeManage/alertPlace/EpmsPoliceInformationList', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-29 16:18:32', 'admin', '2021-02-22 17:28:32', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355068031256129537', '1352494133276602370', '警情关联场所', '/placeManage/alertConPlace/index', 'placeManage/alertConPlace/index', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-29 16:19:29', 'admin', '2021-02-22 17:28:42', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1355068873191354369', '1352494133276602370', '违规人员聚集场所', '/placeManage/agaRulePeoPlace/index', 'placeManage/agaRulePeoPlace/index', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-01-29 16:22:50', 'admin', '2021-02-22 17:28:55', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1356182340380102657', '1352494133276602370', '区级工作台', '/zhgl/workingArea', 'zhgl/workingArea', NULL, NULL, 1, NULL, '1', 20.00, 0, 'layout', 1, 1, 0, 1, NULL, 'admin', '2021-02-01 18:07:21', 'admin', '2021-04-13 17:47:33', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1362645745408303106', '1352494133276602370', '数据台账', '/dataAccount', 'layouts/RouteView', NULL, NULL, 1, NULL, '1', 18.00, 0, 'table', 1, 1, 0, 1, NULL, 'admin', '2021-02-19 14:10:37', 'admin', '2021-04-14 09:22:46', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1362646565625421826', '1352494133276602370', '从业人员', '/dataAccount/employee/EpmsEmployeeList', 'dataAccount/employee/EpmsEmployeeList', NULL, NULL, 1, NULL, '1', 2.00, 0, 'team', 1, 1, 1, 0, NULL, 'admin', '2021-02-19 14:13:53', 'admin', '2021-05-08 15:23:51', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1362697858335367170', '1352494133276602370', '从业人员详情', '/dataAccount/employee/EpmsEmployeeDetail', 'dataAccount/employee/EpmsEmployeeDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-02-19 17:37:42', 'admin', '2021-04-20 09:39:00', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363668716277121025', '1352494133276602370', '场所警情', '/dataAccount/policeInfor/EpmsPoliceInformationList', 'dataAccount/policeInfor/EpmsPoliceInformationList', NULL, NULL, 1, NULL, '1', 4.00, 0, 'exception', 1, 0, 1, 0, NULL, 'admin', '2021-02-22 09:55:32', 'admin', '2021-05-08 15:24:13', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363669283233775617', '1352494133276602370', '警情台账详情', '/dataAccount/policeInfor/EpmsPoliceInformationDetail', 'dataAccount/policeInfor/EmpsPoliceInformationDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-02-22 09:57:48', 'admin', '2021-04-20 09:39:14', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363690139645296641', '1352494133276602370', '查处案件', '/dataAccount/placeCase/EpmsPlaceCaseList', 'dataAccount/placeCase/EpmsPlaceCaseList', NULL, NULL, 1, NULL, '1', 3.00, 0, 'reconciliation', 1, 0, 1, 0, NULL, 'admin', '2021-02-22 11:20:40', 'admin', '2021-05-08 15:24:03', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363778225809018882', '1352494133276602370', '娱乐场所', '/dataAccount/place/EpmsEplaceList', 'dataAccount/place/EpmsEplaceList', NULL, NULL, 1, NULL, '1', 1.00, 0, 'gold', 1, 0, 1, 0, NULL, 'admin', '2021-02-22 17:10:41', 'admin', '2021-05-08 15:23:36', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363786004946845697', '', 'demo', '/snsi/demo', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 999.00, 0, 'robot', 1, 0, 0, 1, NULL, 'admin', '2021-02-22 17:41:36', 'admin', '2021-04-12 14:59:10', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363787175921025026', '', '系统功能', '/kinth/tools', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 998.00, 0, 'deployment-unit', 1, 0, 0, 0, NULL, 'admin', '2021-02-22 17:46:15', 'admin', '2021-02-25 16:47:25', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363787356334870530', '', '统计分析', '/newDashBoard', 'layouts/RouteView', NULL, NULL, 0, NULL, '1', 2.00, 0, 'pie-chart', 1, 0, 0, 0, NULL, 'admin', '2021-02-22 17:46:58', 'admin', '2021-06-16 16:11:18', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363787785034682369', '1363787356334870530', '娱乐场所统计', '/newDashBoard/placeBoard', 'newDashBoard/placeBoard', NULL, NULL, 1, NULL, '1', 1.00, 0, 'line-chart', 1, 1, 0, 0, NULL, 'admin', '2021-02-22 17:48:41', 'admin', '2021-06-16 16:11:26', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363788030581821441', '1363787356334870530', '从业人员统计', '/newDashBoard/employeeBoard', 'newDashBoard/employeeBoard', NULL, NULL, 1, NULL, '1', 2.00, 0, 'team', 1, 1, 0, 0, NULL, 'admin', '2021-02-22 17:49:39', 'admin', '2021-06-16 16:11:31', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363788239365885953', '1363787356334870530', '查处场所统计', '/newDashBoard/caseBoard', 'newDashBoard/caseBoard', NULL, NULL, 1, NULL, '1', 3.00, 0, 'profile', 1, 1, 0, 1, NULL, 'admin', '2021-02-22 17:50:29', 'admin', '2021-07-26 11:10:57', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363788465711501313', '1363787356334870530', '场所警情统计', '/newDashBoard/policeBoard', 'newDashBoard/policeBoard', NULL, NULL, 1, NULL, '1', 4.00, 0, 'bell', 1, 1, 0, 1, NULL, 'admin', '2021-02-22 17:51:23', 'admin', '2021-07-26 11:11:05', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363788709073408001', '1363787356334870530', '查处人员统计', '/newDashBoard/illegalBoard', 'newDashBoard/illegalBoard', NULL, NULL, 1, NULL, '1', 5.00, 0, 'thunderbolt', 1, 1, 0, 1, NULL, 'admin', '2021-02-22 17:52:21', 'admin', '2021-05-14 16:54:46', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363789269281427458', '1363787356334870530', '任务工单统计', '/newDashBoard/taskBoard', 'newDashBoard/taskBoard', NULL, NULL, 1, NULL, '1', 6.00, 0, 'schedule', 1, 1, 0, 1, NULL, 'admin', '2021-02-22 17:54:34', 'admin', '2021-07-26 11:11:19', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1363789423879278594', '1363787356334870530', '大屏展示', '/newDashBoard/bigScreen', 'newDashBoard/bigScreen', NULL, NULL, 1, NULL, '1', 7.00, 0, 'select', 1, 1, 0, 1, NULL, 'admin', '2021-02-22 17:55:11', 'admin', '2021-05-14 16:55:07', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364028179320324097', '1352494133276602370', '工单分派', '/zhgl/areaCheckOrder', 'zhgl/areaCheckOrder', NULL, NULL, 1, NULL, '1', 8.00, 0, 'file-text', 1, 0, 0, 0, NULL, 'admin', '2021-02-23 09:43:55', 'admin', '2021-05-08 14:46:22', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364029092554522625', '1352494133276602370', '场所检查管理', '/placeCheckManage/checkLog/main', 'layouts/RouteView', NULL, NULL, 1, NULL, '1', 7.00, 0, 'cluster', 1, 0, 0, 1, NULL, 'admin', '2021-02-23 09:47:33', 'admin', '2021-04-14 10:19:22', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364029328190521346', '1352494133276602370', '检查记录', '/placeCheckManage/checkLog/index', 'placeCheckManage/checkLog/index', NULL, NULL, 1, NULL, '1', 9.00, 0, 'search', 1, 1, 0, 0, NULL, 'admin', '2021-02-23 09:48:29', 'admin', '2021-04-14 10:15:21', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364029550501216257', '1352494133276602370', '任务工单(派出所)', '/zhgl/pcsOrderStatus', 'zhgl/pcsOrderStatus', NULL, NULL, 1, NULL, '1', 7.00, 0, 'file-text', 1, 1, 0, 0, NULL, 'admin', '2021-02-23 09:49:22', 'admin', '2021-04-14 09:18:59', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364034672643981313', '1352494133276602370', '任务工单(区)', '/zhgl/quOrderStatus', 'zhgl/quOrderStatus', NULL, NULL, 1, NULL, '1', 8.00, 0, 'file-search', 1, 1, 0, 0, NULL, 'admin', '2021-02-23 10:09:43', 'admin', '2021-04-14 09:19:25', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364035263843713025', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '检查模板', '/placeCheckManage/checkModel/index', 'placeCheckManage/checkModel/index', NULL, NULL, 1, NULL, '1', 15.00, 0, 'pic-right', 1, 1, 0, 0, NULL, 'admin', '2021-02-23 10:12:04', 'admin', '2021-04-14 09:20:20', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364035443095683074', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '检查标准库', '/placeCheckManage/checkBasic/index', 'placeCheckManage/checkBasic/index', NULL, NULL, 1, NULL, '1', 16.00, 0, 'box-plot', 1, 1, 0, 0, NULL, 'admin', '2021-02-23 10:12:47', 'admin', '2021-04-14 09:20:33', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1364836877101944834', '1363778225809018882', '备案登记按钮', '', NULL, NULL, NULL, 2, 'placeRecord:register', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-02-25 15:17:24', 'admin', '2021-04-19 14:56:31', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1365121229387976706', '1352494133276602370', '区级检查任务工单', '/zhgl/quOrderStatus', 'zhgl/quOrderStatus', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-02-26 10:07:18', 'admin', '2021-02-26 11:28:55', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1365121428801966081', '1352494133276602370', '派出所检查任务工单', '/zhgl/pcsOrderStatus', 'zhgl/pcsOrderStatus', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-02-26 10:08:06', 'admin', '2021-02-26 11:29:07', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1365121615331053569', '1352494133276602370', '我的检查任务工单', '/zhgl/selfOrderStatus', 'zhgl/selfOrderStatus', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-02-26 10:08:50', 'admin', '2021-02-26 11:35:02', 0, 1, '1', 0);
INSERT INTO `sys_permission` VALUES ('1365219538786004993', '1352494133276602370', '新增场所案件', '/dataAccount/placeCase/EpmsPlaceCaseAdd', 'dataAccount/placeCase/EpmsPlaceCaseAdd', NULL, NULL, 1, NULL, '1', 22.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-02-26 16:37:57', 'admin', '2021-04-20 09:38:43', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1366589115528810497', '1364029092554522625', '检查详情', '/placeCheckManage/checkLog/InspectionDetails', 'placeCheckManage/checkLog/InspectionDetails', NULL, NULL, 1, NULL, '1', 6.00, 0, NULL, 1, 0, 0, 1, NULL, 'admin', '2021-03-02 11:20:10', 'admin', '2021-03-06 10:07:52', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1366644506933497857', '1352494133276602370', '派单', '/zhgl/distributeOrder', 'zhgl/distributeOrder', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-02 15:00:16', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1366659049126531074', '1364029092554522625', '检查模板新增', '/placeCheckManage/checkModel/checkTemplate', 'placeCheckManage/checkModel/checkTemplate', NULL, NULL, 1, NULL, '1', 7.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-02 15:58:03', 'admin', '2021-03-06 10:08:04', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367017154985230337', '1364029092554522625', '检查项新增', '/placeCheckManage/checkBasic/checkItems', 'placeCheckManage/checkBasic/checkItems', NULL, NULL, 1, NULL, '1', 8.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-03 15:41:02', 'admin', '2021-03-06 10:08:14', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367054813246795778', '1352494133276602370', '场所案件详情', '/dataAccount/placeCase/EpmsPlaceCaseDetail', 'dataAccount/placeCase/EpmsPlaceCaseDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-03 18:10:41', 'admin', '2021-04-20 09:39:34', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367404298724462594', '1352494133276602370', '场所民警指派', '/placeManage/placePolice/distributePlace', 'placeManage/placePolice/distributePlace', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-04 17:19:25', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367738567783174145', '1352494133276602370', '违规人员聚集详情', '/placeManage/agaRulePeoPlace/agaRulePeoPlaceDetail', 'placeManage/agaRulePeoPlace/agaRulePeoPlaceDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-05 15:27:41', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367754236795985922', '1352494133276602370', '长期未检查场所详情', '/placeManage/longNoChePlace/longNoChePlaceDetail', 'placeManage/longNoChePlace/longNoChePlaceDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-05 16:29:56', 'admin', '2021-03-06 10:29:52', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367781985082318850', '1352494133276602370', '数据不完整场所详情', '/placeManage/dataNoCompPlace/dataNoComPlaceDetail', 'placeManage/dataNoCompPlace/dataNoComPlaceDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-05 18:20:12', 'admin', '2021-04-19 16:09:21', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1367a93f2c410b169faa7abcbad2f77c', '6e73eb3c26099c191bf03852ee1310a1', '基本设置', '/account/settings/BaseSetting', 'account/settings/BaseSetting', 'account-settings-base', NULL, 1, 'BaseSettings', NULL, NULL, 0, NULL, 1, 1, NULL, 1, NULL, NULL, '2018-12-26 18:58:35', 'admin', '2019-03-20 12:57:31', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('1368026522048425986', '1352494133276602370', '长期未上报场所详情', '/placeManage/longNoRepPlace/longNoRepPlaceDetail', 'placeManage/longNoRepPlace/longNoRepPlaceDetail', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-06 10:31:54', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369112584086863874', '1352494718902104065', '工单监控详情', '/zhgl/pcsOrderStatus', NULL, NULL, NULL, 2, 'work:orderDetail', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 10:27:32', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369175459471966209', '1363778225809018882', '场所备案导出按钮', NULL, NULL, NULL, NULL, 2, 'placeRecord:export', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 14:37:22', 'admin', '2021-04-19 14:56:46', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369177643546722305', '1363690139645296641', '场所案件导入', NULL, NULL, NULL, NULL, 2, 'case:import', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 14:46:03', 'admin', '2021-03-09 14:47:28', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369177766485966849', '1363690139645296641', '场所案件添加', NULL, NULL, NULL, NULL, 2, 'case:add', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 14:46:32', 'admin', '2021-03-09 14:47:45', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369178249476849666', '1363690139645296641', '场所案件删除', NULL, NULL, NULL, NULL, 2, 'case:delete', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 14:48:27', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369179574805610498', '1352494133276602370', '发起工单按钮', NULL, NULL, NULL, NULL, 2, 'order:startOrder', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 14:53:43', 'admin', '2021-04-22 13:57:16', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369181192385085441', '1363668716277121025', '关联场所按钮', NULL, NULL, NULL, NULL, 2, 'alarm:relatePlace', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:00:09', 'admin', '2021-05-12 12:00:51', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369181520107028482', '1363668716277121025', '警情添加按钮', NULL, NULL, NULL, NULL, 2, 'alarm:add', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:01:27', 'admin', '2021-05-12 12:00:26', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369182508775784449', '1363668716277121025', '警情导入按钮', NULL, NULL, NULL, NULL, 2, 'alarm:import', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:05:23', 'admin', '2021-05-12 12:00:40', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369182857435693057', '1363668716277121025', '警情删除按钮', NULL, NULL, NULL, NULL, 2, 'alarm:delete', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:06:46', 'admin', '2021-05-12 12:01:02', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369185744417079297', '1364028179320324097', '派单按钮', NULL, NULL, NULL, NULL, 2, 'checkOrder:assignOrder', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:18:14', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369186515858636801', '1355066391568478209', '分配民警按钮', NULL, NULL, NULL, NULL, 2, 'police:assignPolice', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:21:18', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369188318532747265', '1366589115528810497', '处理结论提交', NULL, NULL, NULL, NULL, 2, 'workCheck:checkResultSubmit', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:28:28', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369192052553826306', '1363668716277121025', '警情修改按钮', NULL, NULL, NULL, NULL, 2, 'alarm:modify', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:43:18', 'admin', '2021-05-12 12:01:13', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1369193331711688705', '1352494718902104065', '个人监控详情', NULL, NULL, NULL, NULL, 2, 'work:selfOrderDetail', '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-03-09 15:48:23', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1373077679531786242', '1363787175921025026', '配置任务', 'http://39.108.10.15:8802/spiap-conf-admin/conf', 'layouts/IframePageView', NULL, NULL, 1, NULL, '1', 2.00, 0, 'snippets', 1, 1, 0, 0, NULL, 'admin', '2021-03-20 09:03:24', 'admin', '2021-03-24 10:30:24', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1373825089271799810', '1352494133276602370', '选择场所', '/placeManage/placePolice/selectPlaceList', 'placeManage/placePolice/selectPlaceList', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-03-22 10:33:20', 'admin', '2021-03-22 10:34:50', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1373921900945879041', '1363787175921025026', '表单设计器', '/online/desform', 'modules/online/desform/DesignFormList', NULL, NULL, 1, NULL, '1', 2.00, 0, 'pic-left', 1, 1, 1, 0, NULL, 'admin', '2021-03-22 16:58:02', 'admin', '2021-03-24 10:31:05', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380419651301011457', '1363787356334870530', '任务工单统计分析(区)', '/newDashBoard/taskBoardPcs', 'newDashBoard/taskBoardPcs', NULL, NULL, 1, NULL, '1', 8.00, 0, 'schedule', 1, 1, 0, 1, NULL, 'admin', '2021-04-09 15:17:46', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380435899892101122', '1363787356334870530', '区场所综合统计报表', '/newDashBoard/report/placeReportQu', 'newDashBoard/report/placeReportQu', NULL, NULL, 1, NULL, '1', 20.00, 0, 'shop', 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:22:20', 'admin', '2021-04-09 16:22:56', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380436404789833729', '1363787356334870530', '派出所场所综合统计报表', '/newDashBoard/report/placeReportPcs', 'newDashBoard/report/placeReportPcs', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:24:21', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380436720105025538', '1363787356334870530', '区从业人员综合统计报表', '/newDashBoard/report/employeeReportQu', 'newDashBoard/report/employeeReportQu', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:25:36', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380437019842572289', '1363787356334870530', '派出所从业人员综合统计报表', '/newDashBoard/report/employeeReportPcs', 'newDashBoard/report/employeeReportPcs', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:26:47', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380443347302477826', '1363787356334870530', '派出所查处场所统计', '/newDashBoard/report/caseReportPcs', 'newDashBoard/report/caseReportPcs', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:51:56', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380443763469709314', '1363787356334870530', '派出所场所警情统计', '/newDashBoard/report/alertReportPcs', 'newDashBoard/report/alertReportPcs', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:53:35', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1380444072921264130', '1363787356334870530', '派出所查处人员统计', '/newDashBoard/report/punishPeoplePcs', 'newDashBoard/report/punishPeoplePcs', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-04-09 16:54:49', 'admin', '2021-04-25 10:23:49', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1381508018327506946', '', '综合查询', '/bigDataSearch', 'bigDataSearch/index', NULL, NULL, 0, NULL, '1', 6.00, 0, 'laptop', 1, 1, 0, 0, NULL, 'admin', '2021-04-12 15:22:33', 'admin', '2021-06-16 16:11:48', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1390203365526478849', 'd7d6e2e4e2934f2c9385a623fd98c6f3', 'app版本管理', '/appVersion/SysVersionUpdateList', 'appVersion/SysVersionUpdateList', NULL, NULL, 1, NULL, '1', 30.00, 0, 'android', 1, 1, 0, 0, NULL, 'admin', '2021-05-06 15:14:46', 'admin', '2021-05-06 15:26:44', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1393141451055112194', '1363787356334870530', '区查处场所统计', '/newDashBoard/report/caseReportQu', 'newDashBoard/report/caseReportQu', NULL, NULL, 1, NULL, '1', 20.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-05-14 17:49:40', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1394550350647881730', '1352494133276602370', '大清查行动', '/placeManage/inventoryAction/checkCleanUpList', 'placeManage/inventoryAction/checkCleanUpList', NULL, NULL, 1, NULL, '1', 18.00, 0, 'schedule', 1, 1, 0, 0, NULL, 'admin', '2021-05-18 15:08:08', 'admin', '2021-05-18 15:13:02', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1394568512298774530', '1352494133276602370', '新增大清查', '/placeManage/inventoryAction/checkCleanUpAdd', 'placeManage/inventoryAction/checkCleanUpAdd', NULL, NULL, 1, NULL, '1', 30.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-05-18 16:20:18', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1394915519026561026', '1363787356334870530', '区场所警情统计', '/newDashBoard/report/alertReportQu', 'newDashBoard/report/alertReportQu', NULL, NULL, 1, NULL, '1', 30.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-05-19 15:19:11', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1394951661184794625', '1352494133276602370', '预警提醒', '/placeManage/alarmTips/EpmsViolatorsWarnList', 'placeManage/alarmTips/EpmsViolatorsWarnList', NULL, NULL, 1, NULL, '1', 19.00, 0, 'alert', 1, 1, 0, 0, NULL, 'admin', '2021-05-19 17:42:48', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1394952285725048833', '1352494133276602370', '区完成情况统计', '/placeManage/inventoryAction/checkUpReportQu', 'placeManage/inventoryAction/checkUpReportQu', NULL, NULL, 1, NULL, '1', 30.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-05-19 17:45:17', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('1394952671265472514', '1352494133276602370', '派出所完成情况统计', '/placeManage/inventoryAction/checkUpReportPcs', 'placeManage/inventoryAction/checkUpReportPcs', NULL, NULL, 1, NULL, '1', 32.00, 0, NULL, 1, 1, 0, 1, NULL, 'admin', '2021-05-19 17:46:48', NULL, NULL, 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('190c2b43bec6a5f7a4194a85db67d96a', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '角色管理', '/isystem/roleUserList', 'system/RoleUserList', NULL, NULL, 1, NULL, NULL, 1.20, 0, 'solution', 1, 1, 0, 0, NULL, 'admin', '2019-04-17 15:13:56', 'admin', '2021-03-24 10:14:50', 0, 1, NULL, 0);
INSERT INTO `sys_permission` VALUES ('1a0811914300741f4e11838ff37a1d3a', '3f915b2769fc80648e92d04e84ca059d', '手机号禁用', NULL, NULL, NULL, NULL, 2, 'user:form:phone', '2', 1.00, 0, NULL, 0, 1, NULL, 0, NULL, 'admin', '2019-05-11 17:19:30', 'admin', '2019-05-11 18:00:22', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('200006f0edf145a2b50eacca07585451', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（应用）', '/list/search/application', 'examples/list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-12 14:02:51', 'admin', '2019-02-12 14:14:01', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('22d6a3d39a59dd7ea9a30acfa6bfb0a5', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO动态表单', '/online/df/:table/:id', 'modules/online/cgform/auto/OnlineDynamicForm', NULL, NULL, 1, NULL, NULL, 9.00, 0, NULL, 0, 1, NULL, 1, NULL, 'admin', '2019-04-22 15:15:43', 'admin', '2019-04-30 18:18:26', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('265de841c58907954b8877fb85212622', '2a470fc0c3954d9dbb61de6d80846549', '图片拖拽排序', '/jeecg/imgDragSort', 'jeecg/ImgDragSort', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 10:43:08', 'admin', '2019-04-25 10:46:26', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('277bfabef7d76e89b33062b16a9a5020', 'e3c13679c73a4f829bcff2aba8fd68b1', '基础表单', '/form/base-form', 'examples/form/BasicForm', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-02-26 17:02:08', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('2a470fc0c3954d9dbb61de6d80846549', '1363786004946845697', '常见案例', '/jeecg', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 7.00, 0, 'qrcode', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:47:27', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('2aeddae571695cd6380f6d6d334d6e7d', 'f0675b52d89100ee88472b6800754a08', '布局统计报表', '/report/ArchivesStatisticst', 'jeecg/report/ArchivesStatisticst', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-03 18:32:48', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('2dbbafa22cda07fa5d169d741b81fe12', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '在线文档', '{{ window._CONFIG[\'domianURL\'] }}/doc.html', 'layouts/IframePageView', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-01-30 10:00:01', 'admin', '2020-09-09 14:48:58', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('2e42e3835c2b44ec9f7bc26c146ee531', '1363786004946845697', '人员管理', '/result', 'layouts/PageView', NULL, NULL, 1, NULL, NULL, 8.00, 0, 'check-circle-o', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:47:10', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('339329ed54cf255e1f9392e84f136901', '2a470fc0c3954d9dbb61de6d80846549', 'helloworld', '/jeecg/helloworld', 'jeecg/helloworld', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-02-15 16:24:56', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('3f915b2769fc80648e92d04e84ca059d', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '用户管理', '/isystem/user', 'system/UserList', NULL, NULL, 1, NULL, NULL, 1.10, 0, 'user', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-03-24 10:09:34', 0, 1, NULL, 0);
INSERT INTO `sys_permission` VALUES ('3fac0d3c9cd40fa53ab70d4c583821f8', '2a470fc0c3954d9dbb61de6d80846549', '分屏', '/jeecg/splitPanel', 'jeecg/SplitPanel', NULL, NULL, 1, NULL, NULL, 6.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 16:27:06', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4148ec82b6acd69f470bea75fe41c357', '2a470fc0c3954d9dbb61de6d80846549', '单表模型示例', '/jeecg/jeecgDemoList', 'jeecg/JeecgDemoList', 'DemoList', NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, 0, 0, NULL, NULL, '2018-12-28 15:57:30', 'jeecg', '2020-05-14 22:09:34', 0, 1, NULL, 0);
INSERT INTO `sys_permission` VALUES ('418964ba087b90a84897b62474496b93', '540a2936940846cb98114ffb0d145cb8', '查询表格', '/list/query-list', 'examples/list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4356a1a67b564f0988a484f5531fd4d9', '2a470fc0c3954d9dbb61de6d80846549', '内嵌Table', '/jeecg/TableExpandeSub', 'jeecg/TableExpandeSub', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-04 22:48:13', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('45c966826eeff4c99b8f8ebfe74511fc', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '部门管理', '/isystem/depart', 'system/DepartList', NULL, NULL, 1, NULL, NULL, 1.40, 0, 'usergroup-add', 1, 1, 0, 0, NULL, 'admin', '2019-01-29 18:47:40', 'admin', '2021-03-24 10:11:01', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('4875ebe289344e14844d8e3ea1edd73f', '1363786004946845697', '详情页', '/profile', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 8.00, 0, 'profile', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:46:56', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('4f66409ef3bbd69c1d80469d6e2a885e', '6e73eb3c26099c191bf03852ee1310a1', '账户绑定', '/account/settings/binding', 'account/settings/Binding', NULL, NULL, 1, 'BindingSettings', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 19:01:20', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('4f84f9400e5e92c95f05b554724c2b58', '540a2936940846cb98114ffb0d145cb8', '角色列表', '/list/role-list', 'examples/list/RoleList', NULL, NULL, 1, NULL, NULL, 4.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('53a9230444d33de28aa11cc108fb1dba', '5c8042bd6c601270b2bbd9b20bccc68b', '我的消息', '/isps/userAnnouncement', 'system/UserAnnouncementList', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-04-19 10:16:00', 'admin', '2019-12-25 09:54:34', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('54097c6a3cf50fad0793a34beff1efdf', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线表单', '/online/cgformList/:code', 'modules/online/cgform/auto/OnlCgformAutoList', NULL, NULL, 1, NULL, NULL, 9.00, 0, NULL, 1, 1, NULL, 1, NULL, 'admin', '2019-03-19 16:03:06', 'admin', '2019-04-30 18:19:03', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('540a2936940846cb98114ffb0d145cb8', '1363786004946845697', '列表页', '/list', 'layouts/PageView', NULL, '/list/query-list', 1, NULL, NULL, 9.00, 0, 'table', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:44:33', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('54dd5457a3190740005c1bfec55b1c34', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '菜单管理', '/isystem/permission', 'system/PermissionList', NULL, NULL, 1, NULL, NULL, 1.30, 0, 'bars', 1, 1, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-03-24 10:10:05', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('58857ff846e61794c69208e9d3a85466', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '日志管理', '/isystem/log', 'system/LogList', NULL, NULL, 1, NULL, NULL, 11.00, 0, 'read', 1, 1, 0, 0, NULL, NULL, '2018-12-26 10:11:18', 'admin', '2021-06-08 09:59:14', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('58b9204feaf07e47284ddb36cd2d8468', '2a470fc0c3954d9dbb61de6d80846549', '图片翻页', '/jeecg/imgTurnPage', 'jeecg/ImgTurnPage', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 11:36:42', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('5c2f42277948043026b7a14692456828', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '我的部门', '/isystem/departUserList', 'system/DepartUserList', NULL, NULL, 1, NULL, NULL, 2.00, 0, 'user-add', 1, 1, 0, 0, NULL, 'admin', '2019-04-17 15:12:24', 'admin', '2021-03-24 10:15:52', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('5c8042bd6c601270b2bbd9b20bccc68b', '1363787175921025026', '消息中心', '/message', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 6.00, 0, 'message', 1, 0, 0, 0, NULL, 'admin', '2019-04-09 11:05:04', 'admin', '2021-02-22 17:48:00', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('6531cf3421b1265aeeeabaab5e176e6d', 'e3c13679c73a4f829bcff2aba8fd68b1', '分步表单', '/form/step-form', 'examples/form/stepForm/StepForm', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('655563cd64b75dcf52ef7bcdd4836953', '2a470fc0c3954d9dbb61de6d80846549', '图片预览', '/jeecg/ImagPreview', 'jeecg/ImagPreview', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-17 11:18:45', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('65a8f489f25a345836b7f44b1181197a', 'c65321e57b7949b7a975313220de0422', '403', '/exception/403', 'exception/403', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('6ad53fd1b220989a8b71ff482d683a5a', '2a470fc0c3954d9dbb61de6d80846549', '一对多Tab示例', '/jeecg/tablist/JeecgOrderDMainList', 'jeecg/tablist/JeecgOrderDMainList', NULL, NULL, 1, NULL, NULL, 2.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-20 14:45:09', 'admin', '2019-02-21 16:26:21', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('6e73eb3c26099c191bf03852ee1310a1', '717f6bee46f44a3897eca9abd6e2ec44', '个人设置', '/account/settings/BaseSetting', 'account/settings/Index', NULL, NULL, 1, NULL, NULL, 2.00, 1, NULL, 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-19 09:41:05', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('700b7f95165c46cc7a78bf227aa8fed3', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '性能监控', '/monitor', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2019-04-02 11:34:34', 'admin', '2020-09-09 14:48:51', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('717f6bee46f44a3897eca9abd6e2ec44', '1363787175921025026', '个人页', '/account', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 9.00, 0, 'user', 1, 0, 0, 1, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:47:47', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('73678f9daa45ed17a3674131b03432fb', '540a2936940846cb98114ffb0d145cb8', '权限列表', '/list/permission-list', 'examples/list/PermissionList', NULL, NULL, 1, NULL, NULL, 5.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('7960961b0063228937da5fa8dd73d371', '2a470fc0c3954d9dbb61de6d80846549', 'JEditableTable示例', '/jeecg/JEditableTable', 'jeecg/JeecgEditableTableExample', NULL, NULL, 1, NULL, NULL, 2.10, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-03-22 15:22:18', 'admin', '2019-12-25 09:48:16', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('7ac9eb9ccbde2f7a033cd4944272bf1e', '540a2936940846cb98114ffb0d145cb8', '卡片列表', '/list/card', 'examples/list/CardList', NULL, NULL, 1, NULL, NULL, 7.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('841057b8a1bef8f6b4b20f9a618a7fa6', '08e6b9dc3c04489c8e1ff2ce6f105aa4', '数据日志', '/sys/dataLog-list', 'system/DataLogList', NULL, NULL, 1, NULL, NULL, 2.10, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-03-11 19:26:49', 'admin', '2020-09-09 14:48:32', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('882a73768cfd7f78f3a37584f7299656', '6e73eb3c26099c191bf03852ee1310a1', '个性化设置', '/account/settings/custom', 'account/settings/Custom', NULL, NULL, 1, 'CustomSettings', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 19:00:46', NULL, '2018-12-26 21:13:25', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8b3bff2eee6f1939147f5c68292a1642', '700b7f95165c46cc7a78bf227aa8fed3', '服务器信息', '/monitor/SystemInfo', 'modules/monitor/SystemInfo', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 11:39:19', 'admin', '2019-04-02 15:40:02', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8d1ebd663688965f1fd86a2f0ead3416', '700b7f95165c46cc7a78bf227aa8fed3', 'Redis监控', '/monitor/redis/info', 'modules/monitor/RedisInfo', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 13:11:33', 'admin', '2019-05-07 15:18:54', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('8d4683aacaa997ab86b966b464360338', 'e41b69c57a941a3bbcce45032fe57605', 'Online表单开发', '/online/cgform', 'modules/online/cgform/OnlCgformHeadList', NULL, NULL, 1, NULL, NULL, 1.00, 0, 'file', 1, 0, 0, 0, NULL, 'admin', '2019-03-12 15:48:14', 'admin', '2021-03-24 14:11:59', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('8fb8172747a78756c11916216b8b8066', '717f6bee46f44a3897eca9abd6e2ec44', '工作台', '/dashboard/workplace', 'dashboard/Workplace', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-04-02 11:45:02', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('944abf0a8fc22fe1f1154a389a574154', '5c8042bd6c601270b2bbd9b20bccc68b', '消息管理', '/modules/message/sysMessageList', 'modules/message/SysMessageList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-09 11:27:53', 'admin', '2019-04-09 19:31:23', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('9502685863ab87f0ad1134142788a385', '', '首页', '/dashboard/analysis', 'zhgl/working', NULL, NULL, 0, NULL, NULL, 0.00, 0, 'home', 1, 1, 1, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-05-08 15:05:25', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('97c8629abc7848eccdb6d77c24bb3ebb', '700b7f95165c46cc7a78bf227aa8fed3', '磁盘监控', '/monitor/Disk', 'modules/monitor/DiskMonitoring', NULL, NULL, 1, NULL, NULL, 6.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 14:30:06', 'admin', '2019-05-05 14:37:14', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('9a90363f216a6a08f32eecb3f0bf12a3', '2a470fc0c3954d9dbb61de6d80846549', '场所备案登记', '/jeecg/SelectDemo', 'jeecg/SelectDemo', NULL, NULL, 1, NULL, NULL, 0.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-03-19 11:19:05', 'admin', '2021-01-29 09:47:09', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('9cb91b8851db0cf7b19d7ecc2a8193dd', '1939e035e803a99ceecb6f5563570fb2', '我的任务表单', '/modules/bpm/task/form/FormModule', 'modules/bpm/task/form/FormModule', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-03-08 16:49:05', 'admin', '2019-03-08 18:37:56', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('9fe26464838de2ea5e90f2367e35efa0', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO在线报表', '/online/cgreport/:code', 'modules/online/cgreport/auto/OnlCgreportAutoList', 'onlineAutoList', NULL, 1, NULL, NULL, 9.00, 0, NULL, 1, 1, NULL, 1, NULL, 'admin', '2019-03-12 11:06:48', 'admin', '2019-04-30 18:19:10', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('a400e4f4d54f79bf5ce160ae432231af', '2a470fc0c3954d9dbb61de6d80846549', '百度', 'http://www.baidu.com', 'layouts/IframePageView', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-01-29 19:44:06', 'admin', '2019-02-15 16:25:02', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('ae4fed059f67086fd52a73d913cf473d', '540a2936940846cb98114ffb0d145cb8', '内联编辑表格', '/list/edit-table', 'examples/list/TableInnerEditList', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('aedbf679b5773c1f25e9f7b10111da73', '08e6b9dc3c04489c8e1ff2ce6f105aa4', 'SQL监控', '{{ window._CONFIG[\'domianURL\'] }}/druid/', 'layouts/IframePageView', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2019-01-30 09:43:22', 'admin', '2020-09-09 14:48:38', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('b1cb0a3fedf7ed0e4653cb5a229837ee', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '定时任务', '/isystem/QuartzJobList', 'system/QuartzJobList', NULL, NULL, 1, NULL, NULL, 10.00, 0, 'loading', 1, 1, 0, 0, NULL, NULL, '2019-01-03 09:38:52', 'admin', '2021-06-08 09:59:53', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('b3c824fc22bd953e2eb16ae6914ac8f9', '4875ebe289344e14844d8e3ea1edd73f', '高级详情页', '/profile/advanced', 'examples/profile/advanced/Advanced', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('b4dfc7d5dd9e8d5b6dd6d4579b1aa559', 'c65321e57b7949b7a975313220de0422', '500', '/exception/500', 'exception/500', NULL, NULL, 1, NULL, NULL, 3.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('b6bcee2ccc854052d3cc3e9c96d90197', '71102b3b87fb07e5527bbd2c530dd90a', '加班申请', '/modules/extbpm/joa/JoaOvertimeList', 'modules/extbpm/joa/JoaOvertimeList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-03 15:33:10', 'admin', '2019-04-03 15:34:48', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('c431130c0bc0ec71b0a5be37747bb36a', '2a470fc0c3954d9dbb61de6d80846549', '一对多JEditable', '/jeecg/JeecgOrderMainListForJEditableTable', 'jeecg/JeecgOrderMainListForJEditableTable', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-03-29 10:51:59', 'admin', '2019-04-04 20:09:39', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('c65321e57b7949b7a975313220de0422', '1363787175921025026', '异常页', '/exception', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 8.00, 0, 'warning', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:46:39', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('c6cf95444d80435eb37b2f9db3971ae6', '2a470fc0c3954d9dbb61de6d80846549', '数据回执模拟', '/jeecg/InterfaceTest', 'jeecg/InterfaceTest', NULL, NULL, 1, NULL, NULL, 6.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-19 16:02:23', 'admin', '2019-02-21 16:25:45', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('cc50656cf9ca528e6f2150eba4714ad2', '4875ebe289344e14844d8e3ea1edd73f', '基础详情页', '/profile/basic', 'examples/profile/basic/Index', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d07a2c87a451434c99ab06296727ec4f', '700b7f95165c46cc7a78bf227aa8fed3', 'JVM信息', '/monitor/JvmInfo', 'modules/monitor/JvmInfo', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-01 23:07:48', 'admin', '2019-04-02 11:37:16', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d2bbf9ebca5a8fa2e227af97d2da7548', 'c65321e57b7949b7a975313220de0422', '404', '/exception/404', 'exception/404', NULL, NULL, 1, NULL, NULL, 2.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('d7d6e2e4e2934f2c9385a623fd98c6f3', '', '系统管理', '/isystem', 'layouts/RouteView', NULL, NULL, 0, NULL, NULL, 7.00, 0, 'setting', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-04-14 09:14:19', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('d86f58e7ab516d3bc6bfb1fe10585f97', '717f6bee46f44a3897eca9abd6e2ec44', '个人中心', '/account/center', 'account/center/Index', NULL, NULL, 1, NULL, NULL, 1.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('de13e0f6328c069748de7399fcc1dbbd', 'fb07ca05a3e13674dbf6d3245956da2e', '搜索列表（项目）', '/list/search/project', 'examples/list/TableList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-12 14:01:40', 'admin', '2019-02-12 14:14:18', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e08cb190ef230d5d4f03824198773950', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '系统通告', '/isystem/annountCement', 'system/SysAnnouncementList', NULL, NULL, 1, 'annountCement', NULL, 6.00, 0, 'schedule', 1, 1, 0, 0, NULL, NULL, '2019-01-02 17:23:01', 'admin', '2021-03-24 10:19:38', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('e1979bb53e9ea51cecc74d86fd9d2f64', '2a470fc0c3954d9dbb61de6d80846549', 'PDF预览', '/jeecg/jeecgPdfView', 'jeecg/JeecgPdfView', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-25 10:39:35', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e3c13679c73a4f829bcff2aba8fd68b1', '1363786004946845697', '表单页', '/form', 'layouts/PageView', NULL, NULL, 1, NULL, NULL, 9.00, 0, 'form', 1, 0, 0, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2021-02-22 17:44:09', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('e41b69c57a941a3bbcce45032fe57605', '1363787175921025026', '在线开发', '/online', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 1.00, 0, 'cloud', 1, 0, 0, 0, NULL, 'admin', '2019-03-08 10:43:10', 'admin', '2021-03-24 11:13:28', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('e5973686ed495c379d829ea8b2881fc6', 'e3c13679c73a4f829bcff2aba8fd68b1', '高级表单', '/form/advanced-form', 'examples/form/advancedForm/AdvancedForm', NULL, NULL, 1, NULL, NULL, 3.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('e6bfd1fcabfd7942fdd05f076d1dad38', '2a470fc0c3954d9dbb61de6d80846549', '打印测试', '/jeecg/PrintDemo', 'jeecg/PrintDemo', NULL, NULL, 1, NULL, NULL, 3.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-19 15:58:48', 'admin', '2019-05-07 20:14:39', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('ebb9d82ea16ad864071158e0c449d186', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '分类字典', '/isys/category', 'system/SysCategoryList', NULL, NULL, 1, NULL, '1', 5.20, 0, 'layout', 1, 1, 0, 0, NULL, 'admin', '2019-05-29 18:48:07', 'admin', '2021-03-24 10:18:46', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('ec8d607d0156e198b11853760319c646', '6e73eb3c26099c191bf03852ee1310a1', '安全设置', '/account/settings/security', 'account/settings/Security', NULL, NULL, 1, 'SecuritySettings', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 18:59:52', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('f0675b52d89100ee88472b6800754a08', '1363786004946845697', '统计报表', '/report', 'layouts/RouteView', NULL, NULL, 1, NULL, NULL, 1.10, 0, 'bar-chart', 1, 0, 0, 0, NULL, 'admin', '2019-04-03 18:32:02', 'admin', '2021-02-22 17:50:49', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('f1cb187abf927c88b89470d08615f5ac', 'd7d6e2e4e2934f2c9385a623fd98c6f3', '数据字典', '/isystem/dict', 'system/DictList', NULL, NULL, 1, NULL, NULL, 5.00, 0, 'fund', 1, 1, 0, 0, NULL, NULL, '2018-12-28 13:54:43', 'admin', '2021-03-24 10:18:15', 0, 0, NULL, 0);
INSERT INTO `sys_permission` VALUES ('f23d9bfff4d9aa6b68569ba2cff38415', '540a2936940846cb98114ffb0d145cb8', '标准列表', '/list/basic-list', 'examples/list/StandardList', NULL, NULL, 1, NULL, NULL, 6.00, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2018-12-25 20:34:38', NULL, NULL, 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('f2849d3814fc97993bfc519ae6bbf049', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO复制表单', '/online/copyform/:code', 'modules/online/cgform/OnlCgformCopyList', NULL, NULL, 1, NULL, '1', 1.00, 0, 'copy', 1, 1, 0, 1, NULL, 'admin', '2019-08-29 16:05:37', 'admin', '2021-03-24 14:12:34', 0, 0, '1', 0);
INSERT INTO `sys_permission` VALUES ('f780d0d3083d849ccbdb1b1baee4911d', '5c8042bd6c601270b2bbd9b20bccc68b', '模板管理', '/modules/message/sysMessageTemplateList', 'modules/message/SysMessageTemplateList', NULL, NULL, 1, NULL, NULL, 1.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-09 11:50:31', 'admin', '2019-04-12 10:16:34', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fb07ca05a3e13674dbf6d3245956da2e', '540a2936940846cb98114ffb0d145cb8', '搜索列表', '/list/search', 'examples/list/search/SearchLayout', NULL, '/list/search/article', 1, NULL, NULL, 8.00, 0, NULL, 1, 0, NULL, 0, NULL, NULL, '2018-12-25 20:34:38', 'admin', '2019-02-12 15:09:13', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fb367426764077dcf94640c843733985', '2a470fc0c3954d9dbb61de6d80846549', '一对多示例', '/jeecg/JeecgOrderMainList', 'jeecg/JeecgOrderMainList', NULL, NULL, 1, NULL, NULL, 2.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-02-15 16:24:11', 'admin', '2019-02-18 10:50:14', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fba41089766888023411a978d13c0aa4', 'e41b69c57a941a3bbcce45032fe57605', 'AUTO树表单列表', '/online/cgformTreeList/:code', 'modules/online/cgform/auto/OnlCgformTreeList', NULL, NULL, 1, NULL, '1', 9.00, 0, NULL, 1, 1, NULL, 1, NULL, 'admin', '2019-05-21 14:46:50', 'admin', '2019-06-11 13:52:52', 0, 0, '1', NULL);
INSERT INTO `sys_permission` VALUES ('fc810a2267dd183e4ef7c71cc60f4670', '700b7f95165c46cc7a78bf227aa8fed3', '请求追踪', '/monitor/HttpTrace', 'modules/monitor/HttpTrace', NULL, NULL, 1, NULL, NULL, 4.00, 0, NULL, 1, 1, NULL, 0, NULL, 'admin', '2019-04-02 09:46:19', 'admin', '2019-04-02 11:37:27', 0, 0, NULL, NULL);
INSERT INTO `sys_permission` VALUES ('fedfbf4420536cacc0218557d263dfea', '6e73eb3c26099c191bf03852ee1310a1', '新消息通知', '/account/settings/notification', 'account/settings/Notification', NULL, NULL, 1, 'NotificationSettings', NULL, NULL, NULL, '', 1, 1, NULL, NULL, NULL, NULL, '2018-12-26 19:02:05', NULL, NULL, 0, 0, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `role_name` varchar(200) DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(100) NOT NULL COMMENT '角色编码',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uniq_sys_role_role_code` (`role_code`) USING BTREE,
  KEY `idx_sr_role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES ('1354963405357920257', '游客', 'guest', NULL, 'admin', '2021-01-29 09:23:44', 'admin', '2021-01-29 17:40:39');
INSERT INTO `sys_role` VALUES ('f6817f48af4fb3af11b9e8bf182f618b', '管理员', 'admin', '管理员', NULL, '2018-12-21 18:03:39', 'admin', '2019-05-20 11:40:26');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` varchar(32) NOT NULL,
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色id',
  `permission_id` varchar(32) DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) DEFAULT NULL COMMENT '数据权限ids',
  `operate_date` datetime DEFAULT NULL COMMENT '操作时间',
  `operate_ip` varchar(20) DEFAULT NULL COMMENT '操作ip',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_group_role_per_id` (`role_id`,`permission_id`) USING BTREE,
  KEY `index_group_role_id` (`role_id`) USING BTREE,
  KEY `index_group_per_id` (`permission_id`) USING BTREE,
  KEY `idx_srp_role_per_id` (`role_id`,`permission_id`) USING BTREE,
  KEY `idx_srp_role_id` (`role_id`) USING BTREE,
  KEY `idx_srp_permission_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色权限表';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_permission` VALUES ('0254c0b25694ad5479e6d6935bbc176e', 'f6817f48af4fb3af11b9e8bf182f618b', '944abf0a8fc22fe1f1154a389a574154', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0c2d2db76ee3aa81a4fe0925b0f31365', 'f6817f48af4fb3af11b9e8bf182f618b', '024f1fd1283dc632458976463d8984e1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0c6b8facbb1cc874964c87a8cf01e4b1', 'f6817f48af4fb3af11b9e8bf182f618b', '841057b8a1bef8f6b4b20f9a618a7fa6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0c6e1075e422972083c3e854d9af7851', 'f6817f48af4fb3af11b9e8bf182f618b', '08e6b9dc3c04489c8e1ff2ce6f105aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0e1469997af2d3b97fff56a59ee29eeb', 'f6817f48af4fb3af11b9e8bf182f618b', 'e41b69c57a941a3bbcce45032fe57605', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('0f861cb988fdc639bb1ab943471f3a72', 'f6817f48af4fb3af11b9e8bf182f618b', '97c8629abc7848eccdb6d77c24bb3ebb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1185039870491439105', 'f6817f48af4fb3af11b9e8bf182f618b', '1174506953255182338', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1185039870529187841', 'f6817f48af4fb3af11b9e8bf182f618b', '1174590283938041857', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1197431682208206850', 'f6817f48af4fb3af11b9e8bf182f618b', '1192318987661234177', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1197795315916271617', 'f6817f48af4fb3af11b9e8bf182f618b', '109c78a583d4693ce2f16551b7786786', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1197795316268593154', 'f6817f48af4fb3af11b9e8bf182f618b', '9fe26464838de2ea5e90f2367e35efa0', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209423580355481602', 'f6817f48af4fb3af11b9e8bf182f618b', '190c2b43bec6a5f7a4194a85db67d96a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1209654501558046722', 'f6817f48af4fb3af11b9e8bf182f618b', 'f2849d3814fc97993bfc519ae6bbf049', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1231590078632955905', 'f6817f48af4fb3af11b9e8bf182f618b', '1224641973866467330', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1231590078658121729', 'f6817f48af4fb3af11b9e8bf182f618b', '1209731624921534465', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1231590078662316033', 'f6817f48af4fb3af11b9e8bf182f618b', '1229674163694841857', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1260928399955836929', 'f6817f48af4fb3af11b9e8bf182f618b', '1260928341675982849', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1269526122208522241', 'f6817f48af4fb3af11b9e8bf182f618b', '1267412134208319489', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('126ea9faebeec2b914d6d9bef957afb6', 'f6817f48af4fb3af11b9e8bf182f618b', 'f1cb187abf927c88b89470d08615f5ac', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1281494164924653569', 'f6817f48af4fb3af11b9e8bf182f618b', '1280350452934307841', NULL, '2020-07-10 15:43:13', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1281494684632473602', 'f6817f48af4fb3af11b9e8bf182f618b', '1265162119913824258', NULL, '2020-07-10 15:45:16', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1355056123744370690', '1354963405357920257', '1205097455226462210', NULL, '2021-01-29 15:32:10', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1355056123756953601', '1354963405357920257', '1205098241075453953', NULL, '2021-01-29 15:32:10', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1355056123756953602', '1354963405357920257', '1205306106780364802', NULL, '2021-01-29 15:32:10', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467202072577', '1354963405357920257', 'f0675b52d89100ee88472b6800754a08', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467214655489', '1354963405357920257', '2aeddae571695cd6380f6d6d334d6e7d', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467214655490', '1354963405357920257', '020b06793e4de2eee0007f603000c769', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467214655491', '1354963405357920257', '1232123780958064642', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467218849794', '1354963405357920257', '2a470fc0c3954d9dbb61de6d80846549', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467223044097', '1354963405357920257', '9a90363f216a6a08f32eecb3f0bf12a3', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467223044098', '1354963405357920257', '1287715272999944193', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467227238402', '1354963405357920257', '1287715783966834689', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467231432705', '1354963405357920257', '1287716451494510593', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467231432706', '1354963405357920257', '1287718919049691137', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467235627010', '1354963405357920257', '1287718938179911682', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467235627011', '1354963405357920257', '1287718956957810689', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467235627012', '1354963405357920257', '1166535831146504193', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467239821314', '1354963405357920257', '4148ec82b6acd69f470bea75fe41c357', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467239821315', '1354963405357920257', '4356a1a67b564f0988a484f5531fd4d9', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467239821316', '1354963405357920257', '655563cd64b75dcf52ef7bcdd4836953', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467244015617', '1354963405357920257', '6ad53fd1b220989a8b71ff482d683a5a', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467244015618', '1354963405357920257', 'fb367426764077dcf94640c843733985', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467248209922', '1354963405357920257', '7960961b0063228937da5fa8dd73d371', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467248209923', '1354963405357920257', '043780fa095ff1b2bec4dc406d76f023', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467248209924', '1354963405357920257', '0620e402857b8c5b605e1ad9f4b89350', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467248209925', '1354963405357920257', 'c431130c0bc0ec71b0a5be37747bb36a', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467252404225', '1354963405357920257', 'e1979bb53e9ea51cecc74d86fd9d2f64', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467252404226', '1354963405357920257', 'e6bfd1fcabfd7942fdd05f076d1dad38', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467252404227', '1354963405357920257', '265de841c58907954b8877fb85212622', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467252404228', '1354963405357920257', '339329ed54cf255e1f9392e84f136901', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467256598529', '1354963405357920257', '58b9204feaf07e47284ddb36cd2d8468', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467256598530', '1354963405357920257', 'a400e4f4d54f79bf5ce160ae432231af', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467256598531', '1354963405357920257', '3fac0d3c9cd40fa53ab70d4c583821f8', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467256598532', '1354963405357920257', 'c6cf95444d80435eb37b2f9db3971ae6', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467256598533', '1354963405357920257', '1280464606292099074', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467260792833', '1354963405357920257', '1260922988733255681', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1356175467260792834', '1354963405357920257', '1260923256208216065', NULL, '2021-02-01 17:40:03', '192.168.3.19');
INSERT INTO `sys_role_permission` VALUES ('1363788086630252546', 'f6817f48af4fb3af11b9e8bf182f618b', '277bfabef7d76e89b33062b16a9a5020', NULL, '2021-02-22 17:49:52', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_role_permission` VALUES ('1363790125909245953', 'f6817f48af4fb3af11b9e8bf182f618b', '1363787175921025026', NULL, '2021-02-22 17:57:59', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_role_permission` VALUES ('1373077953499529217', 'f6817f48af4fb3af11b9e8bf182f618b', '1373077679531786242', NULL, '2021-03-20 09:04:29', '120.230.84.201');
INSERT INTO `sys_role_permission` VALUES ('1373920438447624193', 'f6817f48af4fb3af11b9e8bf182f618b', '1304032910990495745', NULL, '2021-03-22 16:52:13', '192.168.3.122');
INSERT INTO `sys_role_permission` VALUES ('1373920438456012802', 'f6817f48af4fb3af11b9e8bf182f618b', '1235823781053313025', NULL, '2021-03-22 16:52:13', '192.168.3.122');
INSERT INTO `sys_role_permission` VALUES ('1373922016092106753', 'f6817f48af4fb3af11b9e8bf182f618b', '1373921900945879041', NULL, '2021-03-22 16:58:30', '113.67.156.192');
INSERT INTO `sys_role_permission` VALUES ('1384024567646945282', 'f6817f48af4fb3af11b9e8bf182f618b', '1260929666434318338', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567663722498', 'f6817f48af4fb3af11b9e8bf182f618b', '1260931366557696001', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567667916802', 'f6817f48af4fb3af11b9e8bf182f618b', '1260933542969458689', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567667916803', 'f6817f48af4fb3af11b9e8bf182f618b', '1a0811914300741f4e11838ff37a1d3a', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567676305409', 'f6817f48af4fb3af11b9e8bf182f618b', '1363786004946845697', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567676305410', 'f6817f48af4fb3af11b9e8bf182f618b', 'f0675b52d89100ee88472b6800754a08', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567680499714', 'f6817f48af4fb3af11b9e8bf182f618b', '2aeddae571695cd6380f6d6d334d6e7d', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567680499715', 'f6817f48af4fb3af11b9e8bf182f618b', '020b06793e4de2eee0007f603000c769', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567680499716', 'f6817f48af4fb3af11b9e8bf182f618b', '1232123780958064642', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567684694018', 'f6817f48af4fb3af11b9e8bf182f618b', '1205097455226462210', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567684694019', 'f6817f48af4fb3af11b9e8bf182f618b', '1205098241075453953', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567684694020', 'f6817f48af4fb3af11b9e8bf182f618b', '1205306106780364802', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384024567684694021', 'f6817f48af4fb3af11b9e8bf182f618b', '2a470fc0c3954d9dbb61de6d80846549', NULL, '2021-04-19 14:02:26', '119.129.128.28');
INSERT INTO `sys_role_permission` VALUES ('1384052493654323202', 'f6817f48af4fb3af11b9e8bf182f618b', '9502685863ab87f0ad1134142788a385', NULL, '2021-04-19 15:53:24', '113.67.158.136');
INSERT INTO `sys_role_permission` VALUES ('1385403428452376577', '1354963405357920257', '9502685863ab87f0ad1134142788a385', NULL, '2021-04-23 09:21:32', '119.129.115.175');
INSERT INTO `sys_role_permission` VALUES ('1390203466198163457', 'f6817f48af4fb3af11b9e8bf182f618b', '1390203365526478849', NULL, '2021-05-06 15:15:10', '113.67.156.192');
INSERT INTO `sys_role_permission` VALUES ('1402081820085911554', 'f6817f48af4fb3af11b9e8bf182f618b', '9a90363f216a6a08f32eecb3f0bf12a3', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820094300161', 'f6817f48af4fb3af11b9e8bf182f618b', '1287715272999944193', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820094300162', 'f6817f48af4fb3af11b9e8bf182f618b', '1287715783966834689', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820094300163', 'f6817f48af4fb3af11b9e8bf182f618b', '1287716451494510593', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820094300164', 'f6817f48af4fb3af11b9e8bf182f618b', '1287718919049691137', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820098494465', 'f6817f48af4fb3af11b9e8bf182f618b', '1287718938179911682', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820098494466', 'f6817f48af4fb3af11b9e8bf182f618b', '1287718956957810689', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820102688769', 'f6817f48af4fb3af11b9e8bf182f618b', '1166535831146504193', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820102688770', 'f6817f48af4fb3af11b9e8bf182f618b', '4148ec82b6acd69f470bea75fe41c357', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820102688771', 'f6817f48af4fb3af11b9e8bf182f618b', '4356a1a67b564f0988a484f5531fd4d9', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820102688772', 'f6817f48af4fb3af11b9e8bf182f618b', '655563cd64b75dcf52ef7bcdd4836953', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883073', 'f6817f48af4fb3af11b9e8bf182f618b', '6ad53fd1b220989a8b71ff482d683a5a', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883074', 'f6817f48af4fb3af11b9e8bf182f618b', 'fb367426764077dcf94640c843733985', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883075', 'f6817f48af4fb3af11b9e8bf182f618b', '7960961b0063228937da5fa8dd73d371', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883076', 'f6817f48af4fb3af11b9e8bf182f618b', '043780fa095ff1b2bec4dc406d76f023', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883077', 'f6817f48af4fb3af11b9e8bf182f618b', '0620e402857b8c5b605e1ad9f4b89350', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883078', 'f6817f48af4fb3af11b9e8bf182f618b', 'c431130c0bc0ec71b0a5be37747bb36a', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883079', 'f6817f48af4fb3af11b9e8bf182f618b', 'e1979bb53e9ea51cecc74d86fd9d2f64', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820106883080', 'f6817f48af4fb3af11b9e8bf182f618b', 'e6bfd1fcabfd7942fdd05f076d1dad38', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820111077378', 'f6817f48af4fb3af11b9e8bf182f618b', '265de841c58907954b8877fb85212622', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820111077379', 'f6817f48af4fb3af11b9e8bf182f618b', '339329ed54cf255e1f9392e84f136901', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820111077380', 'f6817f48af4fb3af11b9e8bf182f618b', '58b9204feaf07e47284ddb36cd2d8468', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820111077381', 'f6817f48af4fb3af11b9e8bf182f618b', 'a400e4f4d54f79bf5ce160ae432231af', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820111077382', 'f6817f48af4fb3af11b9e8bf182f618b', '3fac0d3c9cd40fa53ab70d4c583821f8', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820111077383', 'f6817f48af4fb3af11b9e8bf182f618b', 'c6cf95444d80435eb37b2f9db3971ae6', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820115271682', 'f6817f48af4fb3af11b9e8bf182f618b', '1280464606292099074', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820115271683', 'f6817f48af4fb3af11b9e8bf182f618b', '1260922988733255681', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('1402081820115271684', 'f6817f48af4fb3af11b9e8bf182f618b', '1260923256208216065', NULL, '2021-06-08 09:55:30', '127.0.0.1');
INSERT INTO `sys_role_permission` VALUES ('154edd0599bd1dc2c7de220b489cd1e2', 'f6817f48af4fb3af11b9e8bf182f618b', '7ac9eb9ccbde2f7a033cd4944272bf1e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('165acd6046a0eaf975099f46a3c898ea', 'f6817f48af4fb3af11b9e8bf182f618b', '4f66409ef3bbd69c1d80469d6e2a885e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1664b92dff13e1575e3a929caa2fa14d', 'f6817f48af4fb3af11b9e8bf182f618b', 'd2bbf9ebca5a8fa2e227af97d2da7548', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('1e47db875601fd97723254046b5bba90', 'f6817f48af4fb3af11b9e8bf182f618b', 'baf16b7174bd821b6bab23fa9abb200d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('20e53c87a785688bdc0a5bb6de394ef1', 'f6817f48af4fb3af11b9e8bf182f618b', '540a2936940846cb98114ffb0d145cb8', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('25491ecbd5a9b34f09c8bc447a10ede1', 'f6817f48af4fb3af11b9e8bf182f618b', 'd07a2c87a451434c99ab06296727ec4f', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2779cdea8367fff37db26a42c1a1f531', 'f6817f48af4fb3af11b9e8bf182f618b', 'fef097f3903caf3a3c3a6efa8de43fbb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('2ad37346c1b83ddeebc008f6987b2227', 'f6817f48af4fb3af11b9e8bf182f618b', '8d1ebd663688965f1fd86a2f0ead3416', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('38a2e55db0960262800576e34b3af44c', 'f6817f48af4fb3af11b9e8bf182f618b', '5c2f42277948043026b7a14692456828', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('3de2a60c7e42a521fecf6fcc5cb54978', 'f6817f48af4fb3af11b9e8bf182f618b', '2d83d62bd2544b8994c8f38cf17b0ddf', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4204f91fb61911ba8ce40afa7c02369f', 'f6817f48af4fb3af11b9e8bf182f618b', '3f915b2769fc80648e92d04e84ca059d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('444126230885d5d38b8fa6072c9f43f8', 'f6817f48af4fb3af11b9e8bf182f618b', 'f780d0d3083d849ccbdb1b1baee4911d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('45a358bb738782d1a0edbf7485e81459', 'f6817f48af4fb3af11b9e8bf182f618b', '0ac2ad938963b6c6d1af25477d5b8b51', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4dab5a06acc8ef3297889872caa74747', 'f6817f48af4fb3af11b9e8bf182f618b', 'ffb423d25cc59dcd0532213c4a518261', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4e0a37ed49524df5f08fc6593aee875c', 'f6817f48af4fb3af11b9e8bf182f618b', 'f23d9bfff4d9aa6b68569ba2cff38415', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4ea403fc1d19feb871c8bdd9f94a4ecc', 'f6817f48af4fb3af11b9e8bf182f618b', '2e42e3835c2b44ec9f7bc26c146ee531', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('4f254549d9498f06f4cc9b23f3e2c070', 'f6817f48af4fb3af11b9e8bf182f618b', '93d5cfb4448f11e9916698e7f462b4b6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('504e326de3f03562cdd186748b48a8c7', 'f6817f48af4fb3af11b9e8bf182f618b', '027aee69baee98a0ed2e01806e89c891', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('520b5989e6fe4a302a573d4fee12a40a', 'f6817f48af4fb3af11b9e8bf182f618b', '6531cf3421b1265aeeeabaab5e176e6d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('54fdf85e52807bdb32ce450814abc256', 'f6817f48af4fb3af11b9e8bf182f618b', 'cc50656cf9ca528e6f2150eba4714ad2', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5d230e6cd2935c4117f6cb9a7a749e39', 'f6817f48af4fb3af11b9e8bf182f618b', 'fc810a2267dd183e4ef7c71cc60f4670', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5de6871fadb4fe1cdd28989da0126b07', 'f6817f48af4fb3af11b9e8bf182f618b', 'a400e4f4d54f79bf5ce160a3432231af', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('5e4015a9a641cbf3fb5d28d9f885d81a', 'f6817f48af4fb3af11b9e8bf182f618b', '2dbbafa22cda07fa5d169d741b81fe12', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('60eda4b4db138bdb47edbe8e10e71675', 'f6817f48af4fb3af11b9e8bf182f618b', 'fb07ca05a3e13674dbf6d3245956da2e', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('660fbc40bcb1044738f7cabdf1708c28', 'f6817f48af4fb3af11b9e8bf182f618b', 'b3c824fc22bd953e2eb16ae6914ac8f9', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('66b202f8f84fe766176b3f51071836ef', 'f6817f48af4fb3af11b9e8bf182f618b', '1367a93f2c410b169faa7abcbad2f77c', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6b605c261ffbc8ac8a98ae33579c8c78', 'f6817f48af4fb3af11b9e8bf182f618b', 'fba41089766888023411a978d13c0aa4', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6c74518eb6bb9a353f6a6c459c77e64b', 'f6817f48af4fb3af11b9e8bf182f618b', 'b4dfc7d5dd9e8d5b6dd6d4579b1aa559', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6daddafacd7eccb91309530c17c5855d', 'f6817f48af4fb3af11b9e8bf182f618b', 'edfa74d66e8ea63ea432c2910837b150', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('6fb4c2142498dd6d5b6c014ef985cb66', 'f6817f48af4fb3af11b9e8bf182f618b', '6e73eb3c26099c191bf03852ee1310a1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7413acf23b56c906aedb5a36fb75bd3a', 'f6817f48af4fb3af11b9e8bf182f618b', 'a4fc7b64b01a224da066bb16230f9c5a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('76a54a8cc609754360bf9f57e7dbb2db', 'f6817f48af4fb3af11b9e8bf182f618b', 'c65321e57b7949b7a975313220de0422', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7a6bca9276c128309c80d21e795c66c6', 'f6817f48af4fb3af11b9e8bf182f618b', '54097c6a3cf50fad0793a34beff1efdf', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('7ca833caa5eac837b7200d8b6de8b2e3', 'f6817f48af4fb3af11b9e8bf182f618b', 'fedfbf4420536cacc0218557d263dfea', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('84eac2f113c23737128fb099d1d1da89', 'f6817f48af4fb3af11b9e8bf182f618b', '03dc3d93261dda19fc86dd7ca486c6cf', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('86060e2867a5049d8a80d9fe5d8bc28b', 'f6817f48af4fb3af11b9e8bf182f618b', '765dd244f37b804e3d00f475fd56149b', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('884f147c20e003cc80ed5b7efa598cbe', 'f6817f48af4fb3af11b9e8bf182f618b', 'e5973686ed495c379d829ea8b2881fc6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8b09925bdc194ab7f3559cd3a7ea0507', 'f6817f48af4fb3af11b9e8bf182f618b', 'ebb9d82ea16ad864071158e0c449d186', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8d154c2382a8ae5c8d1b84bd38df2a93', 'f6817f48af4fb3af11b9e8bf182f618b', 'd86f58e7ab516d3bc6bfb1fe10585f97', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8dd64f65a1014196078d0882f767cd85', 'f6817f48af4fb3af11b9e8bf182f618b', 'e3c13679c73a4f829bcff2aba8fd68b1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('8e3dc1671abad4f3c83883b194d2e05a', 'f6817f48af4fb3af11b9e8bf182f618b', 'b1cb0a3fedf7ed0e4653cb5a229837ee', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('905bf419332ebcb83863603b3ebe30f0', 'f6817f48af4fb3af11b9e8bf182f618b', '8fb8172747a78756c11916216b8b8066', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9380121ca9cfee4b372194630fce150e', 'f6817f48af4fb3af11b9e8bf182f618b', '65a8f489f25a345836b7f44b1181197a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('94911fef73a590f6824105ebf9b6cab3', 'f6817f48af4fb3af11b9e8bf182f618b', '8b3bff2eee6f1939147f5c68292a1642', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9700d20dbc1ae3cbf7de1c810b521fe6', 'f6817f48af4fb3af11b9e8bf182f618b', 'ec8d607d0156e198b11853760319c646', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('980171fda43adfe24840959b1d048d4d', 'f6817f48af4fb3af11b9e8bf182f618b', 'd7d6e2e4e2934f2c9385a623fd98c6f3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('987c23b70873bd1d6dca52f30aafd8c2', 'f6817f48af4fb3af11b9e8bf182f618b', '00a2a0ae65cdca5e93209cdbde97cbe6', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9b2ad767f9861e64a20b097538feafd3', 'f6817f48af4fb3af11b9e8bf182f618b', '73678f9daa45ed17a3674131b03432fb', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('9d980ec0489040e631a9c24a6af42934', 'f6817f48af4fb3af11b9e8bf182f618b', '05b3c82ddb2536a4a5ee1a4c46b5abef', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a034ed7c38c996b880d3e78f586fe0ae', 'f6817f48af4fb3af11b9e8bf182f618b', 'c89018ea6286e852b424466fd92a2ffc', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('a5d25fdb3c62904a8474182706ce11a0', 'f6817f48af4fb3af11b9e8bf182f618b', '418964ba087b90a84897b62474496b93', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('acacce4417e5d7f96a9c3be2ded5b4be', 'f6817f48af4fb3af11b9e8bf182f618b', 'f9d3f4f27653a71c52faa9fb8070fbe7', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ae1852fb349d8513eb3fdc173da3ee56', 'f6817f48af4fb3af11b9e8bf182f618b', '8d4683aacaa997ab86b966b464360338', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('af60ac8fafd807ed6b6b354613b9ccbc', 'f6817f48af4fb3af11b9e8bf182f618b', '58857ff846e61794c69208e9d3a85466', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('b128ebe78fa5abb54a3a82c6689bdca3', 'f6817f48af4fb3af11b9e8bf182f618b', 'aedbf679b5773c1f25e9f7b10111da73', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('bbec16ad016efec9ea2def38f4d3d9dc', 'f6817f48af4fb3af11b9e8bf182f618b', '13212d3416eb690c2e1d5033166ff47a', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('be8e5a9080569e59863f20c4c57a8e22', 'f6817f48af4fb3af11b9e8bf182f618b', '22d6a3d39a59dd7ea9a30acfa6bfb0a5', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('bea2986432079d89203da888d99b3f16', 'f6817f48af4fb3af11b9e8bf182f618b', '54dd5457a3190740005c1bfec55b1c34', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c56fb1658ee5f7476380786bf5905399', 'f6817f48af4fb3af11b9e8bf182f618b', 'de13e0f6328c069748de7399fcc1dbbd', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c6fee38d293b9d0596436a0cbd205070', 'f6817f48af4fb3af11b9e8bf182f618b', '4f84f9400e5e92c95f05b554724c2b58', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('c90b0b01c7ca454d2a1cb7408563e696', 'f6817f48af4fb3af11b9e8bf182f618b', '882a73768cfd7f78f3a37584f7299656', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('cf1feb1bf69eafc982295ad6c9c8d698', 'f6817f48af4fb3af11b9e8bf182f618b', 'a2b11669e98c5fe54a53c3e3c4f35d14', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('cf2ef620217673e4042f695743294f01', 'f6817f48af4fb3af11b9e8bf182f618b', '717f6bee46f44a3897eca9abd6e2ec44', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('cf43895aef7fc684669483ab00ef2257', 'f6817f48af4fb3af11b9e8bf182f618b', '700b7f95165c46cc7a78bf227aa8fed3', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d281a95b8f293d0fa2a136f46c4e0b10', 'f6817f48af4fb3af11b9e8bf182f618b', '5c8042bd6c601270b2bbd9b20bccc68b', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d83282192a69514cfe6161b3087ff962', 'f6817f48af4fb3af11b9e8bf182f618b', '53a9230444d33de28aa11cc108fb1dba', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('d8a5c9079df12090e108e21be94b4fd7', 'f6817f48af4fb3af11b9e8bf182f618b', '078f9558cdeab239aecb2bda1a8ed0d1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('dbc5dd836d45c5bc7bc94b22596ab956', 'f6817f48af4fb3af11b9e8bf182f618b', '1939e035e803a99ceecb6f5563570fb2', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('dc83bb13c0e8c930e79d28b2db26f01f', 'f6817f48af4fb3af11b9e8bf182f618b', '63b551e81c5956d5c861593d366d8c57', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('dc8fd3f79bd85bd832608b42167a1c71', 'f6817f48af4fb3af11b9e8bf182f618b', '91c23960fab49335831cf43d820b0a61', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('de82e89b8b60a3ea99be5348f565c240', 'f6817f48af4fb3af11b9e8bf182f618b', '56ca78fe0f22d815fabc793461af67b8', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('e7467726ee72235baaeb47df04a35e73', 'f6817f48af4fb3af11b9e8bf182f618b', 'e08cb190ef230d5d4f03824198773950', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ec4bc97829ab56afd83f428b6dc37ff6', 'f6817f48af4fb3af11b9e8bf182f618b', '200006f0edf145a2b50eacca07585451', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ec846a3f85fdb6813e515be71f11b331', 'f6817f48af4fb3af11b9e8bf182f618b', '732d48f8e0abe99fe6a23d18a3171cd1', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ecdd72fe694e6bba9c1d9fc925ee79de', 'f6817f48af4fb3af11b9e8bf182f618b', '45c966826eeff4c99b8f8ebfe74511fc', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('ef8bdd20d29447681ec91d3603e80c7b', 'f6817f48af4fb3af11b9e8bf182f618b', 'ae4fed059f67086fd52a73d913cf473d', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('f177acac0276329dc66af0c9ad30558a', 'f6817f48af4fb3af11b9e8bf182f618b', 'c2c356bf4ddd29975347a7047a062440', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fafe73c4448b977fe42880a6750c3ee8', 'f6817f48af4fb3af11b9e8bf182f618b', '9cb91b8851db0cf7b19d7ecc2a8193dd', NULL, NULL, NULL);
INSERT INTO `sys_role_permission` VALUES ('fced905c7598973b970d42d833f73474', 'f6817f48af4fb3af11b9e8bf182f618b', '4875ebe289344e14844d8e3ea1edd73f', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `username` varchar(100) DEFAULT NULL COMMENT '登录账号',
  `realname` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) DEFAULT NULL COMMENT 'md5密码盐',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `index_user_name` (`username`) USING BTREE,
  UNIQUE KEY `uniq_sys_user_username` (`username`) USING BTREE,
  KEY `idx_su_username` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES ('1355044258322608129', 'guest', '游客', 'd66473ec0c9aa54e', 'McOD3eku');
INSERT INTO `sys_user` VALUES ('e9ca23d68d884d4ebb19d07889727dae', 'admin', '管理员', 'cb362cfeefbf3d8d', 'RCGTeGiH');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index2_groupuu_user_id` (`user_id`) USING BTREE,
  KEY `index2_groupuu_ole_id` (`role_id`) USING BTREE,
  KEY `index2_groupuu_useridandroleid` (`user_id`,`role_id`) USING BTREE,
  KEY `idx_sur_user_id` (`user_id`) USING BTREE,
  KEY `idx_sur_role_id` (`role_id`) USING BTREE,
  KEY `idx_sur_user_role_id` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES ('1384806681015443457', '1355044258322608129', '1354963405357920257');
INSERT INTO `sys_user_role` VALUES ('1384754140336390145', 'e9ca23d68d884d4ebb19d07889727dae', 'f6817f48af4fb3af11b9e8bf182f618b');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
